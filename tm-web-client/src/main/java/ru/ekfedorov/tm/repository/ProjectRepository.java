package ru.ekfedorov.tm.repository;

import org.springframework.stereotype.Repository;
import ru.ekfedorov.tm.model.Project;

import java.util.*;

@Repository
public class ProjectRepository {

    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        create();
        create();
        create();
    }

    public void create() {
        Project project = new Project("project", "new");
        projects.put(project.getId(), project);
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

    public List<Project> findAll() {
        return new ArrayList<>(projects.values()) ;
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void save(final Project project) {
        projects.put(project.getId(), project);
    }

}

