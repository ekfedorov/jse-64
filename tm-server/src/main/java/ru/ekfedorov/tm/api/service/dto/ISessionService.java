package ru.ekfedorov.tm.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.dto.Session;
import ru.ekfedorov.tm.enumerated.Role;

public interface ISessionService extends IService<Session> {

    @Nullable Session close(@Nullable Session session);

    @Nullable
    Session open(String login, String password);

    @SneakyThrows
    void remove(@Nullable Session entity);

    void validate(@Nullable Session session);

    void validateAdmin(@Nullable Session session, @Nullable Role role);

}
