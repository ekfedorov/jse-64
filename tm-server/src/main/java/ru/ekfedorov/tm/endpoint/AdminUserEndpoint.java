package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.api.endpoint.IAdminUserEndpoint;
import ru.ekfedorov.tm.api.service.dto.ISessionService;
import ru.ekfedorov.tm.api.service.dto.IUserService;
import ru.ekfedorov.tm.api.service.model.IUserGraphService;
import ru.ekfedorov.tm.dto.Session;
import ru.ekfedorov.tm.dto.User;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.UserIsLockedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public final class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

//    public AdminUserEndpoint(@NotNull final ServiceLocator serviceLocator) {
//        super(serviceLocator);
//    }

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IUserGraphService userGraphService;

    @Override
    @WebMethod
    public void clearUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        userGraphService.clear();
    }

    @Override
    @WebMethod
    public void createUserWithRole(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable final Role role
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        userService.create(login, password, role);
    }

    @Override
    @NotNull
    @WebMethod
    public List<User> findAllUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        return userService.findAll();
    }

    @Override
    @Nullable
    @WebMethod
    public User findUserOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        return userService.findOneById(id).orElse(null);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AccessDeniedException, UserIsLockedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        userGraphService.removeByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        userGraphService.removeOneById(id);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AccessDeniedException {
        sessionService.validateAdmin(session, Role.ADMIN);
        userService.unlockUserByLogin(login);
    }

}
